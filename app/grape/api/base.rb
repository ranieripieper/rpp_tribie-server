module API
  class Base < Grape::API
    prefix Application::Config.api_prefix_path if Application::Config.enabled?(:prefix_api_path)
    format :json

    include API::Helpers::CacheDSL

    helpers API::Helpers::ApplicationHelpers
    helpers API::Helpers::AuthHelpers
    helpers API::Helpers::CacheHelpers

    before do
      set_locale
      set_origin
    end

    mount API::V1::Base
  end
end
