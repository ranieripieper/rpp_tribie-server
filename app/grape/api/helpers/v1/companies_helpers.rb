module API
  module Helpers
    module V1
      module CompaniesHelpers


        # TODO: Move to Service
        def fetch_cnpj_data(number, validate_number = false)
          service = execute_service('CNPJ::FetchService', number)

          response_for_service(service, data: service.document_data)
        end

        def valid_cnpj?(number)
           BRDocuments::CNPJ.valid?(number)
        end

        def invalid_cnpj?(number)
          !valid_cnpj?(number)
        end
      end
    end
  end
end
