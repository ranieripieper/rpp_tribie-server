module API
  module Helpers
    module V1
      module AdminHelpers

        extend Grape::API::Helpers

        def admin_user_for(current_user, params)
          # A supergroup user can access all admins
          return AdminUser.find_by(id: params[:id]) if current_user.supergroup?

          # Others users can access only yourself
          return current_user.admin_user
        end

        def current_user_for(current_user, params)
          # A supergroup user can access all admins
          return User.find_by(id: params[:id]) if current_user.supergroup?

          # Others users can access only yourself
          return current_user
        end

        def offer_for(current_user, params)
          current_user.allowed_offers_scope_to_read.find_by(id: params[:id])
        end

        def authorize_current_user!
          unless current_user.admin_allowed?
            return forbidden_error!('users.not_allowed_to_manage_admin')
          end
        end

        def service_errors(errors)
          return errors unless errors.all? {|error| error.is_a?(Hash) }

          temp_errors = Hash.new {|k,v| k[v] = [] }
          errors.each do |error|
            if error.is_a?(Hash)
              temp_errors.merge!(error)
            else
              error.each do |error_key, error_message|
                temp_errors[error_key] << error_message
                temp_errors[error_key].flatten!
              end
            end
          end

          temp_errors
        end

      end
    end
  end
end
