module API
  module Helpers
    module V1
      module AddressesHelpers

        extend Grape::API::Helpers

        def address_attributes_from_zipcode(zipcode, options = {})
          service = execute_service('Addresses::ZipcodeFetchService', zipcode, options)

          response_for_service(service, data: service.response)
        end

        def normalized_zipcode(zipcode)
          Address.normalize_zipcode(zipcode)
        end

      end
    end
  end
end
