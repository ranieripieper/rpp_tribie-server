# encoding: UTF-8

module API
  module V1
    module Routes
      class UsersMe < API::V1::Base

        helpers API::Helpers::V1::UsersHelpers

        before do
          authenticate_user
        end

        #with_cacheable_endpoints :users do
        namespace :users do
          namespace :me do
            
            desc 'Suspend user account'
            delete :account do
              service = execute_service('Users::DeleteService', current_user, current_user)
              simple_response_for_service(service)
            end
            
            namespace :discomfort do
              route_param :id do
                desc 'Accept talking'
                post :accept do
                  params[:status] = :talking 
                  discomfort = current_user.discomforts_helping.find_by(id: params[:id])
                  service = execute_service('Discomforts::AcceptRejectDiscomfortService', discomfort, current_user, params)
                  response_for_update_service(service, :discomfort, serializer: :discomfort, root: :data)
                end
                
                desc 'Reject talking'
                params do
                  optional :status, values: ::Discomfort::VALID_REJECTED_STATUS.values
                end
                post :reject do
                  if params[:status].nil? or params[:status].blank? then
                    params[:status] = :tribier_rejected
                  end
                  
                  discomfort = current_user.discomforts_helping.find_by(id: params[:id])
                  service = execute_service('Discomforts::AcceptRejectDiscomfortService', discomfort, current_user, params)
                  response_for_update_service(service, :discomfort, serializer: :discomfort, root: :data)
                end
                
              end
            end

            paginated_endpoint do
              desc 'Get all discomforts'
              get :discomforts do
                options = options_for_ordering(params)
                cache_data = pagination_cache_replace_data(current_user.id).concat(options.values)
                respond_with_cacheable 'current_user.paginated_discomforts', cache_data do
                  paginated_discomforts_for(current_user, options)
                end
              end
            end
            desc 'Get current discomforts'
            get :discomfort do
              respond_with_cacheable 'current_user.current_discomfort' do
                discomfort = current_user.discomforts.current
                serialized_object(discomfort, serializer: "Discomfort")
              end
            end

            desc 'Get current discomforts helping'
            get :discomforts_helping do
              respond_with_cacheable 'current_user.current_discomfort' do

                discomfort = current_user.discomforts_helping.current_with_deleted
                serialized_object(discomfort, serializer: "Discomfort")

              end
            end

            desc 'Update ProfileType'
            params do
              requires :user, type: Hash do
                requires :profile_type, values: ::User::VALID_PROFILES_TYPES.values
              end
            end
            put :profile_type do
              service = execute_service('Users::UpdateService', current_user, current_user, params)
              response_for_update_service(service, :user, serializer: :current_user, root: :data)
            end

            desc 'Update User'
            params do
              requires :user, type: Hash do
                optional :name, type: String
                optional :birth_date, type: String
                optional :email, type: String, regexp: User::EMAIL_REGEXP
                optional :password, type: String, regexp: User::PASSWORD_REGEXP
                optional :password_confirmation, type: String, regexp: User::PASSWORD_REGEXP
                optional :profile_type, values: ::User::VALID_PROFILES_TYPES.values
              end
            end
            put do
              service = execute_service('Users::UpdateService', current_user, current_user, params)
              response_for_update_service(service, :user, serializer: :user, root: :data)
            end
          end

        end
      #end
      end
    end
  end
end
