module API
  module V1
    module Routes
      class UsersAuth < API::V1::Base

        helpers API::Helpers::V1::UsersHelpers

        namespace :users do
          namespace :auth do

            desc 'Authenticate an user using email'
            params do
              requires :email, type: String
              requires :password, type: String
            end
            post do
              service = authentication_service
              service.execute

              user_provider_auth_response(service)
            end

            desc 'Authenticate using token (must used for app tests)'
            post :token do
              authenticate_user

              response = user_provider_auth_response(auth_token_validate_service)
              response.merge!(email: current_user.obfuscated_email)
              response
            end

            desc 'Clear user authorizations for token provider'
            delete  do
              authenticate_user
              #puts "authenticate_user"
              token_service = auth_token_validate_service
              success = token_service.auth_token.try(:destroy)
              
              logout_delete_account_params = {}
              logout_delete_account_params[:user_id] = current_user.id
              logout_delete_account_params[:authentication_provider] = authentication_provider
              Workers::V1::LogoutDeleteAccountWorker.perform(true, logout_delete_account_params)
              simple_response_for_service(token_service)
            end
          end

          namespace :password_reset do

            desc 'Send a password reset email to user'
            params do
              requires :user, type: Hash do
                requires :email
              end
            end

            post do
              user  = User.find_by('email = ?', params[:user][:email])

              service = execute_service('Auth::PasswordRecoveryService', user, params)

              response_service = {}

              if service.success?
                response_service.merge!(data: {email: user.obfuscated_email, msg: I18n.t(:"messages.reset_password", email: user.obfuscated_email)})
                response_service.merge!({
                  reset_password_token: service.user.reset_password_token,
                  alert_message: "'reset_password_token' is only returned in staging and development environment"
                }) if in_sandbox_environment?
              end

              response_for_service(service, response_service)
            end

            desc 'Update user password'
            params do
              requires :password
              requires :password_confirmation
            end

            put '/:token' do
              user = User.find_by(reset_password_token: params[:token])

              service  = execute_service('Auth::PasswordUpdateService', user, params)

              response_for_service(service, user_name: service.user_name)
            end
          end

          desc 'Activate an user account'
          post '/activate_account/:token' do
            user = User.not_activated.find_by(activation_token: params[:token])

            service = execute_service('Users::ActivateAccountService', user, user, params)

            response_extra = {
              user_name: service.user_name,
              yet_activated: service.yet_activated?(params[:token])
            }

            response_for_service(service, response_extra, true)
          end

          desc 'Resend account confirmation mail'
          params do
              requires :user, type: Hash do
                requires :email, type: String, allow_blank: false
              end
          end
          post :resend_activation_mail do
            user = User.not_activated.find_by(email: params[:user][:email])

            service = execute_service('Users::ActivateAccountMailerDeliveryService', user, params)

            simple_response_for_service(service)
          end
        end
      end
    end
  end
end
