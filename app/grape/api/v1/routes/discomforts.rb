# encoding: UTF-8
module API
  module V1
    module Routes
      class Discomforts < API::V1::Base

        before do
          authenticate_user
        end

        namespace :discomforts do
          params do
            requires :discomfort, type: Hash do
              requires :title, type: String
              requires :description, type: String
            end
          end
          desc 'Create a new Discomfort'
          post do
            #puts "CurrentUser: #{current_user.name}"
            service = execute_service('Discomforts::CreateService', current_user, params)
            response_for_create_service(service, :discomfort)
          end
          
          route_param :id do      
            desc 'Update Status - finish_talking'
            params do
              requires :discomfort, type: Hash do
                requires :status, values: ::Discomfort::VALID_STATUST_TO_REPORT.values
              end
            end
            put :finish_talking do
              discomfort = Discomfort.find_by(id: params[:id])
              service = execute_service('Discomforts::FinishTalkingService', discomfort, current_user, params)
              response_for_update_service(service, :discomfort, serializer: :discomfort, root: :data)
            end
          end
                
        end
      end
    end
  end
end
