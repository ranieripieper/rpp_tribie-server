class Discomfort < ActiveRecord::Base
  include Accessable
  
  acts_as_paranoid

  scope :current, -> {
    order('created_at DESC').limit(1).first
  }
  
  scope :current_with_deleted, -> {
    with_deleted.order('created_at DESC').limit(1).first
  }

  include DiscomfortConcerns::Basic
  
end
