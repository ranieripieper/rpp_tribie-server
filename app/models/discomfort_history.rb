class DiscomfortHistory < ActiveRecord::Base
  
  include Accessable

  acts_as_paranoid
  
  belongs_to :tribier, class_name: "User"
  belongs_to :discomfort
end
