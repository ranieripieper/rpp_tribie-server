module DiscomfortConcerns
  module Basic

    NICKNAMES = Application::Config.nicknames.dup
    
    VALID_STATUST_USER_TO_REPORT = {
      :mission_accomplished  => "mission_accomplished",
      :tribier_disappeared  => "tribier_disappeared",
      :tribier_talking_nonsense  => "tribier_talking_nonsense"
    }
    
    VALID_STATUST_TRIBIER_TO_REPORT = {
      :user_talking_nonsense  => "user_talking_nonsense",
      :user_disappeared  => "user_disappeared"
    }
    
    VALID_STATUST_TO_REPORT = {
      :mission_accomplished  => "mission_accomplished",
      :user_talking_nonsense  => "user_talking_nonsense",
      :user_disappeared  => "user_disappeared",
      :tribier_disappeared  => "tribier_disappeared",
      :tribier_talking_nonsense  => "tribier_talking_nonsense"
    }
    
    VALID_REJECTED_STATUS = {
      :tribier_rejected => "tribier_rejected",
      :tribier_rejected_with_report => "tribier_rejected_with_report"
    }
    
    VALID_STATUS = {
      :created => "created",
      :waiting_response => "waiting_response",
      :tribier_rejected => "tribier_rejected",
      :tribier_rejected_with_report => "tribier_rejected_with_report",
      :timeout => "timeout",
      :talking  => "talking",
      :mission_accomplished  => "mission_accomplished",
      :user_talking_nonsense  => "user_talking_nonsense",
      :user_disappeared  => "user_disappeared",
      :tribier_disappeared  => "tribier_disappeared",
      :tribier_talking_nonsense  => "tribier_talking_nonsense",
      :tribier_logout  => "tribier_logout",
      :user_logout  => "user_logout",
      :tribier_delete_account  => "tribier_delete_account",
      :user_delete_account  => "user_delete_account",
    }

    extend ActiveSupport::Concern

    included do

      has_many :discomfort_histories, class_name: 'DiscomfortHistory', dependent: :destroy
      belongs_to :user, class_name: "User"
      belongs_to :tribier, class_name: "User"

      before_save  :before_save
      before_create  :before_create
      def ably_channel
        return "discomfort_#{id}_user_#{user_id}_tribier_#{tribier_id}"
      end

      def before_create
        self.status = :created
        self.user_nickname = get_nickname
        create_discomfort_history(:created)
      end

      def before_save
        if not self.tribier_id.nil? and (self.tribier_nickname.nil? or self.tribier_nickname.blank?) then
          self.tribier_nickname = get_nickname
          while self.user_nickname == self.tribier_nickname
            self.tribier_nickname = get_nickname
          end
        end
        if self.status != self.status_was then
          create_discomfort_history(self.status)
        end
      end
        
      def link_tribier(user)
        self.status = :waiting_response
        self.tribier = user
      #create_discomfort_history(:waiting_response)
      end

      def talking?
        return self.status.to_sym == :talking
      end
      
      def finished?
        if self.status.to_sym == :mission_accomplished or
            self.status.to_sym == :user_talking_nonsense or
            self.status.to_sym == :user_disappeared or
            self.status.to_sym == :tribier_disappeared or
            self.status.to_sym == :tribier_talking_nonsense or
            self.status.to_sym == :tribier_logout or
            self.status.to_sym == :user_logout then
            
          return true
          
        end
        return false
      end

      def get_nickname
        eval(NICKNAMES).sample
      end

      def create_discomfort_history(status)
        if self.discomfort_histories.nil? or self.discomfort_histories.blank? then
          self.discomfort_histories = []
        end
        discomfort_history = DiscomfortHistory.new
        discomfort_history.discomfort = self
        discomfort_history.tribier_id = self.tribier_id
        discomfort_history.status = status
        self.discomfort_histories << discomfort_history
      end

    end

  end
end
