module UserConcerns
  module Discomforts

    extend ActiveSupport::Concern

    included do

      has_many :discomforts, class_name: 'Discomfort', dependent: :destroy
      has_many :discomforts_helping, class_name: 'Discomfort', :foreign_key => "tribier_id"
      
      def can_create_discomfort?(discomfort)
        current_discomfort = discomforts.current
        if current_discomfort.nil? or current_discomfort.blank? then
          return true
        end
        return current_discomfort.finished?
      end
      
      def discomforts_helping_unescope
        self.discomforts_helping.unescope
      end

    end
  end
end
