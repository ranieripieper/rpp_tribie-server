class User::UserController < ApplicationController
  
  layout 'external_page'
  
  
  def inactive_account
    service = ::Services::V1::Users::InactivateAccountService.new(nil, params)
    service.execute

    if service.success?
      @msg = "sucesso"
    else
      @msg = "erro #{service.full_errors_messages.to_sentence}"
    end
    render 'user/inactivate_account'
  end

  def update_password
    if not params[:password].nil? and not params[:password_confirmation].nil? then
      #altera a senha
      service = ::Services::V1::Auth::PasswordUpdateService.new(nil, params)
      service.execute
     
      if not service.success? then
        @msg = service.full_errors_messages.to_sentence
      else 
        render 'user/update_password_done' and return
      end
    end
    render 'user/update_password'
  end

end
