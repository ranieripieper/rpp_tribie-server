class ApplicationMailer < ActionMailer::Base
  default from: Application::Config.email_noreply
  layout 'mailer'
end
