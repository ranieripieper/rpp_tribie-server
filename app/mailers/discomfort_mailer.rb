class DiscomfortMailer < ApplicationMailer
  
  def report(discomfort, status)
    @discomfort = discomfort
    @status = status

    mail to: Application::Config.report_mail
  end
end
