class AdminsMailer < ApplicationMailer
  
  def inactive_account(user)
    @user = user

    mail to: Application::Config.report_mail
  end
  
end
