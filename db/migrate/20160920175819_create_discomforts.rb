class CreateDiscomforts < ActiveRecord::Migration
  def change
    create_table :discomforts do |t|
      t.references :user, references: :user, index: true, on_delete: :cascade
      t.references :tribier, references: :user, index: true, on_delete: :cascade
      t.string :status
      t.string :title
      t.string :user_nickname
      t.string :tribier_nickname
      t.text :description
      t.timestamps null: false
      
      #paranoid
      t.datetime     :deleted_at, null: true
    end
  end
end
