class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :password_digest
      t.string :profile_type
      t.date :birth_date
      
      #paranoid
      t.datetime     :deleted_at, null: true
      
      #unlok
      t.boolean     :unlocked, null: true, default: true

      t.timestamps null: false
      
      #auth columns
      t.string :oauth_provider
      t.string :oauth_provider_uid
      t.string :activation_token

      t.datetime :activated_at
      t.datetime :activation_sent_at
      t.datetime :blocked_until

      t.string   :reset_password_token
      t.datetime :reset_password_sent_at
      t.datetime :password_reseted_at

      t.integer :login_attempts
      
      t.datetime :tof_accepted_at
      
    end
    
    add_index :users, :email, where: 'deleted_at IS NULL', unique: true
    add_index :users, :oauth_provider
    add_index :users, :oauth_provider_uid
    add_index :users, [:oauth_provider, :oauth_provider_uid], where: 'deleted_at IS NULL', unique: true
    add_index :users, :reset_password_token, unique: true
    add_index :users, :activation_token, unique: true

    
  end
end
