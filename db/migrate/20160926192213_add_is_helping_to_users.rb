class AddIsHelpingToUsers < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.boolean     :is_helping, null: false, default: false
    end
  end
end
