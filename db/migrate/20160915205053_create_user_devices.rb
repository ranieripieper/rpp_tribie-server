class CreateUserDevices < ActiveRecord::Migration
  def change

    create_table :user_devices do |t|
      t.references :user, index: true
      #t.uuid :identifier, default: 'gen_random_uuid()'
      t.string :identifier
      t.string :token
      t.string :platform
      t.string :installation_id

      t.timestamps null: false
      
      #paranoid
      t.datetime     :deleted_at, null: true
      
    end

    add_index :user_devices, :token
    add_foreign_key :user_devices, :users
  end
end
