class CreateDiscomfortHistories < ActiveRecord::Migration
  def change
    create_table :discomfort_histories do |t|
      t.references :tribier, references: :user, index: true, on_delete: :cascade
      t.references :discomfort, index: true, on_delete: :cascade
      t.string :status
      t.timestamps null: false
      
      #paranoid
      t.datetime     :deleted_at, null: true
    end
  end
end
