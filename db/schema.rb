# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160926192213) do

  create_table "authorizations", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.string   "token",      limit: 255
    t.string   "provider",   limit: 255
    t.datetime "expires_at"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "authorizations", ["provider"], name: "index_authorizations_on_provider", using: :btree
  add_index "authorizations", ["token"], name: "index_authorizations_on_token", using: :btree
  add_index "authorizations", ["user_id", "token", "provider"], name: "index_authorizations_on_user_id_and_token_and_provider", unique: true, using: :btree
  add_index "authorizations", ["user_id"], name: "index_authorizations_on_user_id", using: :btree

  create_table "discomfort_histories", force: :cascade do |t|
    t.integer  "tribier_id",    limit: 4
    t.integer  "discomfort_id", limit: 4
    t.string   "status",        limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.datetime "deleted_at"
  end

  add_index "discomfort_histories", ["discomfort_id"], name: "index_discomfort_histories_on_discomfort_id", using: :btree
  add_index "discomfort_histories", ["tribier_id"], name: "index_discomfort_histories_on_tribier_id", using: :btree

  create_table "discomforts", force: :cascade do |t|
    t.integer  "user_id",          limit: 4
    t.integer  "tribier_id",       limit: 4
    t.string   "status",           limit: 255
    t.string   "title",            limit: 255
    t.string   "user_nickname",    limit: 255
    t.string   "tribier_nickname", limit: 255
    t.text     "description",      limit: 65535
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.datetime "deleted_at"
  end

  add_index "discomforts", ["tribier_id"], name: "index_discomforts_on_tribier_id", using: :btree
  add_index "discomforts", ["user_id"], name: "index_discomforts_on_user_id", using: :btree

  create_table "origins", force: :cascade do |t|
    t.integer  "originable_id",   limit: 4
    t.string   "originable_type", limit: 255
    t.string   "ip",              limit: 255
    t.string   "provider",        limit: 255
    t.string   "user_agent",      limit: 255
    t.string   "locale",          limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "origins", ["originable_id", "originable_type"], name: "index_origins_on_originable_id_and_originable_type", unique: true, using: :btree
  add_index "origins", ["originable_type", "originable_id"], name: "index_origins_on_originable_type_and_originable_id", using: :btree

  create_table "user_devices", force: :cascade do |t|
    t.integer  "user_id",         limit: 4
    t.string   "identifier",      limit: 255
    t.string   "token",           limit: 255
    t.string   "platform",        limit: 255
    t.string   "installation_id", limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.datetime "deleted_at"
  end

  add_index "user_devices", ["token"], name: "index_user_devices_on_token", using: :btree
  add_index "user_devices", ["user_id"], name: "index_user_devices_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "first_name",             limit: 255
    t.string   "last_name",              limit: 255
    t.string   "email",                  limit: 255
    t.string   "password_digest",        limit: 255
    t.string   "profile_type",           limit: 255
    t.date     "birth_date"
    t.datetime "deleted_at"
    t.boolean  "unlocked",                           default: true
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
    t.string   "oauth_provider",         limit: 255
    t.string   "oauth_provider_uid",     limit: 255
    t.string   "activation_token",       limit: 255
    t.datetime "activated_at"
    t.datetime "activation_sent_at"
    t.datetime "blocked_until"
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "password_reseted_at"
    t.integer  "login_attempts",         limit: 4
    t.datetime "tof_accepted_at"
    t.boolean  "is_helping",                         default: false, null: false
  end

  add_index "users", ["activation_token"], name: "index_users_on_activation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["oauth_provider", "oauth_provider_uid"], name: "index_users_on_oauth_provider_and_oauth_provider_uid", unique: true, using: :btree
  add_index "users", ["oauth_provider"], name: "index_users_on_oauth_provider", using: :btree
  add_index "users", ["oauth_provider_uid"], name: "index_users_on_oauth_provider_uid", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "authorizations", "users"
  add_foreign_key "user_devices", "users"
end
