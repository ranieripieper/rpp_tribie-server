
# This file is used by Rack-based servers to start the application.

require ::File.expand_path('../config/environment', __FILE__)
run Rails.application

require 'rack/cors'
require 'grape'

use Rack::Cors do
  # allow all origins in development
  allow do
    origins Application::Config.access_control_allowed_origins
    resource '*',
        :headers => :any,
        :methods => [:get, :post, :delete, :put, :options]
  end
end