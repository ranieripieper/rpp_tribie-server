require 'sidekiq/web'

Rails.application.routes.draw do
  mount API::Base => '/'

  Sidekiq::Web.use Rack::Auth::Basic do |username, password|
    username == Application::Config.sidekiq_username && password == Application::Config.sidekiq_password
  end

  mount Sidekiq::Web, at: '/sidekiq'
  
  
  get 'account/inactive/:token' => 'user/user#inactive_account'
  
  get 'account/update-password/:token' => 'user/user#update_password'
  put 'account/update-password/:token' => 'user/user#update_password', :as => "update_password"
end
