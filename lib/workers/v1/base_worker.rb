module Workers
  module V1
    class BaseWorker

      include Sidekiq::Worker

      sidekiq_options :retry => 5

      sidekiq_retry_in { |count| count * 60 }

      def self.perform(async, options)
        perform_action = !async
        if async
          begin
            perform_async(options)
          rescue Exception => e  
            perform_action = true
          end
        end
        if perform_action
          self.new.perform(options)
        end     
      end
    end
  end
end
