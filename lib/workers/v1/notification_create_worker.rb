module Workers
  module V1
    class NotificationCreateWorker < BaseWorker

      sidekiq_options :retry => 5, queue: :system_notifications

      sidekiq_retry_in { |count| count * 60 }

      def perform(notification_data = {})
        user = User.find_by(id: notification_data[:user_id])

        return false unless user
        #TODO
        #service = ::Services::V1::Users::NotificationCreateService.new(user, notification_data)
        #service.execute
      end

    end
  end
end
