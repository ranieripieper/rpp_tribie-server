module Workers
  module V1
    class AcceptRejectTalkingWorker < BaseWorker

      sidekiq_options :retry => 5, queue: :mailers

      sidekiq_retry_in { |count| count * 60 }

      def perform(options = {})
        options = options.deep_symbolize_keys!
        discomfort = Discomfort.find_by(id: options[:discomfort_id])
        return unless discomfort
        
        event = Application::Config.ably_event_tribier_accept_talking.dup
        message = I18n.t(:"messages.tribier_accept_talking", tribier: discomfort.tribier_nickname)
        if not options[:accept_talking] then
          event = Application::Config.ably_event_tribier_reject_talking.dup
          message = I18n.t(:"messages.tribier_reject_talking", tribier: discomfort.tribier_nickname)
        end
        send_message(discomfort, event, message)
        
      end

      def send_message(discomfort, event, msg)
        params = {}
        params[:channel] = discomfort.user.ably_channel
        params[:event] = event
        params[:message] = {message: msg, channel: discomfort.ably_channel}
        
        service = Services::V1::Ably::SendMessageService.new(params)
        service.execute
      end

      
    end
  end
end
