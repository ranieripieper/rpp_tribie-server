module Workers
  module V1
    class UpdateLoginStatusHistoricWorker < BaseWorker
      
      sidekiq_options :retry => 5, queue: :users

      sidekiq_retry_in { |count| count * 60 }

      def perform(options)
        user = User.find_by(id: options[:user_id])

        if user
          user.update_login_count_from_provider!(options[:provider])
        end
      end
    end
  end
end
