module Workers
  module V1
    class OfferModerationCreateWorker

      include Sidekiq::Worker

      sidekiq_options :retry => 5, queue: :moderations

      sidekiq_retry_in { |count| count * 60 }

      def perform(offer_id, user_id, options)
        record = Offer.find_by(id: offer_id)
        user = User.find_by(id: user_id)

        service = ::Services::V1::Moderations::CreateService.new(record, user, options)
        service.execute
      end
    end
  end
end
