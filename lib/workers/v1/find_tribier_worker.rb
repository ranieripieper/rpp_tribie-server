module Workers
  module V1
    class FindTribierWorker < BaseWorker

      sidekiq_options :retry => 5, queue: :tribier

      sidekiq_retry_in { |count| count * 60 }

      def perform(options = {})
        #verifica se tem mais de X horas que o tribier não aceitou...
        verify_timeout
        
        discomforts = Discomfort.where('status=? OR status=? OR status=? OR status=?', :created, :tribier_rejected, :timeout, :tribier_rejected_with_report )
        discomforts.each do |discomfort|
          find_tribier(discomfort)
        end
      end 
      
      def verify_timeout
        date = DateTime.now
        date = date.advance(:hours => -Application::Config.tribier_time_to_response.dup.to_i)

        discomforts = Discomfort.where('status=? and updated_at < ?', :waiting_response, date )
        discomforts.each do |discomfort|
          if not discomfort.tribier.nil? 
            discomfort.tribier.class.send(:update, discomfort.tribier.id, {"is_helping" => false})
          end
          discomfort.class.send(:update, discomfort.id, {"status" => "timeout"})
          send_message_timeout(discomfort)
        end
      end
      
      def find_tribier(discomfort)
        puts ">>> find_tribier #{discomfort.id}"
        ids = get_ids(discomfort)
        user = User.where('profile_type = ? and is_helping = ? and id not in(?) and activated_at is not null', :tribier_user, false, ids).order("RAND()").first
        puts ">>> find_tribier #{discomfort.id} #{user}"
        return false unless user
        discomfort.link_tribier(user)
        discomfort.tribier.class.send(:update, discomfort.tribier.id, {"is_helping" => true})
        discomfort.save
        send_message_can_help(discomfort)
      end 
      
      private
      
      #envia mensagem para o tribier perguntando se ele pode ajudar
      def send_message_can_help(discomfort)
        msg = I18n.t("messages.tribier_can_helping", {tribier: discomfort.tribier_nickname, user: discomfort.user_nickname})
        params = {}
        params[:channel] = discomfort.tribier.ably_channel
        params[:event] = Application::Config.ably_event_tribier_can_helping.dup
        params[:message] = {message: msg, channel: discomfort.ably_channel, discomfort: ::Serializers::V1::SimpleDiscomfortSerializer.new(discomfort, {}).as_json}

        service = Services::V1::Ably::SendMessageService.new(params)
        
        puts ">>> Services::V1::Ably::SendMessageService #{discomfort.id}"
         
        service.execute
        puts ">>> #{service.success?}"
        if service.success? then
          puts "Mensagem enviada.... #{params[:channel]} - #{params[:event]} "
        else
          puts "Erro ao enviar mensagem.... #{params[:channel]} - #{params[:event]} "
        end
      end
      
      #envia mensagem para o tribier de timeout
      def send_message_timeout(discomfort)   
        return unless discomfort.tribier     
        msg = I18n.t(:"messages.tribier_response_timeout", {tribier: discomfort.tribier_nickname})
        params = {}
        params[:channel] = discomfort.tribier.ably_channel
        params[:event] = Application::Config.ably_event_tribier_response_timeout.dup
        params[:message] = {message: msg, channel: discomfort.ably_channel}
        service = Services::V1::Ably::SendMessageService.new(params)
        service.execute
      end
      
      def get_ids(discomfort)
        ids = []
        if not discomfort.discomfort_histories.nil? then
          discomfort.discomfort_histories.each do |history|
            if history.tribier_id then
              ids.push(history.tribier_id) unless ids.include?(history.tribier_id)
            end
          end
        end
        ids.push(discomfort.user_id) unless ids.include?(discomfort.user_id)
        ids
      end
    end
  end
end
