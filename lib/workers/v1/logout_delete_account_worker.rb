module Workers
  module V1
    class LogoutDeleteAccountWorker < BaseWorker

      sidekiq_options :retry => 5, queue: :mailers

      sidekiq_retry_in { |count| count * 60 }

      def perform(options = {})
        options = options.deep_symbolize_keys!
        user = User.unscoped.find_by(id: options[:user_id])
        return unless user
        
        delete_account = options[:delete_account]
        
        discomfort = user.discomforts.current
        
        if not discomfort.nil? and not discomfort.blank? and !discomfort.finished? and not discomfort.tribier.nil? then
          #envia notificação para o tribier que o usuário saiu...    
          channel = discomfort.tribier.ably_channel
          event = Application::Config.ably_event_user_logout.dup
          message = I18n.t(:"messages.user_logout", user: discomfort.user_nickname)
          
          status = :user_logout
          if delete_account then
            event = Application::Config.ably_event_user_delete_account.dup
            status = :user_delete_account
          end
            
          if discomfort.talking? then
            send_message(discomfort, channel, event, message)
          end
          discomfort.class.send(:update, discomfort.id, {"status" => status})         
        end
        
        #discomforto que o usuário está ajudando....
        discomfort = user.discomforts_helping.current
        if not discomfort.nil? and not discomfort.blank? and !discomfort.finished? then
          channel = discomfort.user.ably_channel
          event = Application::Config.ably_event_tribier_logout.dup
          message = I18n.t(:"messages.tribier_logout", tribier: discomfort.tribier_nickname)
          status = :tribier_logout
          if delete_account then
            event = Application::Config.ably_event_tribier_delete_account.dup
            status = :tribier_delete_account
          end
          
          #envia notificação para o user que o tribier saiu...
          if discomfort.talking? then
            #send message --- 
            send_message(discomfort, channel, event, message)
          end 
          
          discomfort.class.send(:update, discomfort.id, {"status" => status})         
        end
        
        if user.devices then
          user.devices.where(platform: options[:authentication_provider]).each do |device|
            params_device = {}
            params_device[:user_id] = current_user.id
            params_device[:device_id] = device.id
            ::Workers::V1::ParseDeviceDeleteWorker.perform(true, params_device)
          end
        end
              
        user.class.send(:update, user.id, {"is_helping" => false})     
        
        if delete_account then
          clear_discomforts(user)
        end    
      end

      def send_message(discomfort, channel, event, msg)
        params = {}
        params[:channel] = channel
        params[:event] = event
        params[:message] = {message: msg, channel: discomfort.ably_channel, discomfort: ::Serializers::V1::SimpleDiscomfortSerializer.new(discomfort, {}).as_json}
        
        service = Services::V1::Ably::SendMessageService.new(params)
        service.execute
      end
     
      def clear_discomforts(user)
        #apaga os discomforts que não tem tribie
        user.discomforts.destroy_all
      end
      
    end
  end
end
