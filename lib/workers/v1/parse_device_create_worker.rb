module Workers
  module V1
    class ParseDeviceCreateWorker < BaseWorker


      sidekiq_options :retry => 5, queue: :parse

      sidekiq_retry_in { |count| count * 60 }

      def perform(options)
        #user_id, device_data, delete_old_devices = false
        user = ::User.find_by(id: options[:user_id])
        device_data = options[:device_data]
        delete_old_devices = false
        if  options[:delete_old_devices].present?
           delete_old_devices = options[:delete_old_devices]
        end
        if user
          device_data = device_data.deep_symbolize_keys!

          device_platform = device_data[:device] ?
                              device_data[:device].fetch(:platform, nil) :
                              device_data[:platform]

          begin
            if delete_old_devices && device_platform.present?
              if ::UserDevice::VALID_PLATFORMS.values.member?(device_platform)
                user.devices.send(device_platform).each do |device|
                  ::Services::V1::Parse::DeviceDeleteService.new(user, device).execute
                end
              end
            end
          rescue Exception => e
            Rollbar.error(e)
          end
          #TODO
          #service = ::Services::V1::Parse::DeviceCreateService.new(user, device_data)
          #service.execute
        end
      end
    end
  end
end
