module Workers
  module V1
    class FinishTalkingWorker < BaseWorker

      sidekiq_options :retry => 5, queue: :mailers

      sidekiq_retry_in { |count| count * 60 }

      def perform(options = {})
        options = options.deep_symbolize_keys!
        discomfort = Discomfort.find_by(id: options[:discomfort_id])
        return unless discomfort
        
        status = options[:status].to_sym
        
        channel = nil
        event = status.to_s
        message = nil
          
        if ::Discomfort::VALID_STATUST_USER_TO_REPORT.member?(status) then
          #avisa o tribier
          channel = discomfort.tribier.ably_channel
          if status == :mission_accomplished then
             message = I18n.t(:"messages.mission_accomplished", user: discomfort.user_nickname)
          else
            message = I18n.t(:"messages.user_report", user: discomfort.user_nickname)
          end
        elsif ::Discomfort::VALID_STATUST_TRIBIER_TO_REPORT.member?(status) then
          #avisa o usuário
          channel = discomfort.user.ably_channel
          message = I18n.t(:"messages.tribier_report", tribier: discomfort.tribier_nickname)
        end
        
        if not channel.nil? then
          send_message(discomfort, channel, event, message)
        end

        discomfort.tribier.class.send(:update, discomfort.tribier.id, {"is_helping" => false})         
      end

      def send_message(discomfort, channel, event, msg)
        params = {}
        params[:channel] = channel
        params[:event] = event
        params[:message] = {message: msg, channel: discomfort.ably_channel}
        
        puts "send msgg =>>> #{channel} - #{event} - #{params[:message]}"
                
        service = Services::V1::Ably::SendMessageService.new(params)
        service.execute
      end

      
    end
  end
end
