module Serializers
  module V1
    class CurrentUserSerializer < SimpleUserSerializer

      attributes :email,
                 :oauth_provider, :oauth_provider_uid,
                 :name
                 #:terms_of_user_accepted_at, :activated_at, :login_status_historic


      def terms_of_user_accepted_at
        object.tof_accepted_at
      end

    end
  end
end
