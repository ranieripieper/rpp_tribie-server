module Serializers
  module V1
    class UserSerializer < SimpleUserSerializer
      attributes :first_name, :last_name, :name, 
                 :profile_type,
                 :created_at, :birth_date
    end
  end
end
