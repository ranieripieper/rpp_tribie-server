module Serializers
  module V1
    class SimpleUserSerializer < ActiveModel::Serializer

      root false

      attributes :id

    end
  end
end
