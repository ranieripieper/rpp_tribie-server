module Serializers
  module V1
    class PublicDiscomfortSerializer < SimpleDiscomfortSerializer

      has_one :tribie, serializer: SimpleUserSerializer

      has_one :user, serializer: SimpleUserSerializer

    end
  end
end
