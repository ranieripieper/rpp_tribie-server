module Serializers
  module V1
    class SimpleDiscomfortSerializer < ActiveModel::Serializer

      root false

      attributes :id, :title, :description, :status, :user_nickname, :tribier_nickname, :user_id, :tribier_id

    end
  end
end
