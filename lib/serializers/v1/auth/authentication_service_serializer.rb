module Serializers
  module V1
    class Auth::AuthenticationServiceSerializer < ActiveModel::Serializer

      root false

      attributes :data, :auth_data
      
      def data
        {
          id: object.user_id,
          profile_type: object.user.profile_type,
          first_name: object.user.first_name,
          last_name: object.user.last_name,
          full_name: object.user.fullname,
          name: object.user.name,
          birth_date: object.user.birth_date
          #oauth_provider: object.user_oauth_provider,
          #oauth_provider_uid: object.user_oauth_provider_uid,
          #new_user: new_user,
          #account_activated: object.user.account_activated?
        }
      end

      def auth_data
        {
          auth_token: object.user_auth_token,
          provider: object.user_auth_provider,
          expires_at: object.user_auth_expires_at
        }
      end

    end
  end
end
