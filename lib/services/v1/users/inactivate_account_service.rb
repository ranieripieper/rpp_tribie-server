module Services
  module V1
    module Users
      class InactivateAccountService < BaseUpdateService

        WHITELIST_ATTRIBUTES = [
          :token
        ]

        record_type ::User

        def initialize(user, options = {})
          @params = options
          super(user, options)
        end

        def execute
          execute_action do
            success_response if update_record
          end
        end

        def update_record
          @user.deactivate_account!
          @user
        end

        def after_success
          notify_admins
        end

        def notify_admins
          delivery_async_email(AdminsMailer, :inactive_account, user_id: @user.id)
          #send_push_notification_async(@user, :account_confirmated)
          #create_system_notification_async(@user, :account_confirmated, @user)
        end

        def can_execute?
          log.info('Can execute???')
          token = @params[:token]

          log.info("Can execute??? #{@params}")
          if token.nil? or token.blank? then
            bad_request_error('users.invalid_token')
          return false
          end

          log.info('Token: %s' % token)

          @user = User.account_activated.where(activation_token: token).first

          log.info('User: %s' % @user)

          if @user.nil? then
            bad_request_error('users.invalid_token')
          return false
          end
          return true
        end

        def record_attributes_hash
          @options#.fetch(options_key, {})
        end

        def record_attributes_whitelist
          WHITELIST_ATTRIBUTES
        end

        private

        # only user can activate your owned account
        def user_can_update_record?
          return true if @user
        end
      end
    end
  end
end
