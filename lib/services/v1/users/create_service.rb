module Services
  module V1
    module Users
      class CreateService < BaseCreateService

        AUTH_TOKEN_PROVIDER_HTTP_HEADER = Application::Config.auth_token_provider_http_header

        record_type ::User

        concern :Users, :CreateUpdate

        attr_reader :authorization

        delegate :id, :oauth_provider, :oauth_provider_uid,
                 to: :user, prefix: :user

        delegate :token, :provider, :expires_at, to: :authorization, prefix: :user_auth, allow_nil: true

        def initialize(options = {})
          super(nil, options)
          @options = options
        end
      
        def new_user?
          return true
        end

        private
       
        def create_origin?
          new_user?
        end

        def user_can_create_record?
          perform_validations
        end

        def valid_user?
          return true
        end

        def after_success
          after_success_actions
        end

        def after_success_actions
          create_authorization
          execute_async_actions
        end
        
        def save_record
          #verifica se já existe o email com o registro apagado
          user_deleted = get_deleted_user
          if not user_deleted.nil? then
            @record.id = user_deleted.id
            @record.deleted_at = nil
          end
          
          create_update_record(user_deleted)
          
        end

        def create_update_record(user)
          begin
            
            if user.nil? then
              @record.save
            else
              params_update = record_allowed_attributes
              params_update[:deleted_at] = nil
              user.class.unscoped.send(:update, @record.id, params_update)
              @record = user
            end
          rescue => e
            on_save_record_error(e)
          ensure
            return @record.valid?
          end
        end

        def get_deleted_user
          return User.unscoped.where("email = ? ", @record.email).first
        end
        
        def async?
          return Application::Config.enabled?(:update_user_after_signup_async) || @options[:async].eql?(true)
        end

        def auto_confirm_account?
          return Application::Config.enabled?(:auto_confirm_account)
        end
        
        def login_after_register?
          return Application::Config.enabled?(:login_after_register)
        end
        
        def create_authorization
          if auto_confirm_account? then
            @record.activate_account!
          end
          
          if login_after_register? || @record.account_activated? then
            return @authorization ||= @record.authorizations.create(provider: get_current_auth_provider)
          end

          @authorization = nil
        end

        def execute_async_actions
          # try to find `device` key in @options[:device] && @options[:user][:device]
          update_service_options = @options
                                    .slice(:origin, :device)
                                    .merge(@options.fetch(:user, {}).slice(:device))

          update_service_options[:notify_user] = new_user?
          update_service_options[:device] && update_service_options[:device][:origin] = origin_params(@options[:origin])

          update_service_options[:user_id] = @record.id
          
          Workers::V1::UserSignupUpdateWorker.perform(async?, update_service_options)
        end
      end
    end
  end
end
