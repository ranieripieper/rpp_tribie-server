# encoding: UTF-8

module Services
  module V1
    module Users
      class DeleteService < BaseDeleteService

        record_type ::User

        private
        def user_can_delete_record?
          @user == @record
        end

        def before_destroy_record
          clear_authorizations
          logout_delete_account_params = {}
          logout_delete_account_params[:user_id] = @user.id      
          logout_delete_account_params[:delete_account] = true      
          Workers::V1::LogoutDeleteAccountWorker.perform(true, logout_delete_account_params)
        end

        def destroy_record
          @record.destroy
        end

        def after_success
          clear_origin
          clear_authorizations
        end
        
        def clear_authorizations
          @record.authorizations.destroy_all
        end

        def clear_origin
          @record.origin.destroy if @record.origin
        end
        
        def clear_originated_notifications
          delete_all_updating_deleted_at(@user.originated_notifications) # working better than .delete_all
        end
        protected

        def delete_all_updating_deleted_at(collection)
          collection.update_all(deleted_at: Time.zone.now)
        end

      end
    end
  end
end
