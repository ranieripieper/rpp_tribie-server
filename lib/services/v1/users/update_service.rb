module Services
  module V1
    class Users::UpdateService < BaseUpdateService

      WHITELIST_ATTRIBUTES = [
        :profile_type,
        :password_confirmation,
        :first_name,
        :last_name,
        :password,
        :birth_date,
        :name
      ]

      record_type ::User

      def changed_attributes
        changes = super

        changes.push(:password) if changes.exclude?(:password) && changed_password?
        changes.push(:name) if changes.exclude?(:name) && changed_name?
        changes
      end

      def changed_name?
        return false unless @last_record
        @last_record.name != @record.name
      end

      def changed_password?
        return false unless @last_record
        @last_record.password_digest != @record.password_digest
      end

      def record_attributes_hash
        (@options[:user] || {}).to_h
      end

      def record_attributes_whitelist
        WHITELIST_ATTRIBUTES
      end

      def user_can_update_record?
        @user == @record
      end

    end
  end
end
