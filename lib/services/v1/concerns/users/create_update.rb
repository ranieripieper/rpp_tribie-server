module Services::V1::Concerns
  module Users
    module CreateUpdate

      extend ActiveSupport::Concern

      WHITELIST_ATTRIBUTES = [
        :profile_type,
        :tof_accepted,
        :password,
        :password_confirmation,
        :name,
        :birth_date,
        :email
      ]

      DEFAULT_PROVIDER = :web

      included do

        def current_auth_provider
          @options[(Application::Config.auth_token_provider_http_param || :auth_provider).to_sym] || DEFAULT_PROVIDER
        end

        def valid_authentication_provider?
          return false if current_auth_provider.blank?

          Authorization::PROVIDERS.member?(current_auth_provider.to_s)
        end

        def perform_validations
          unless valid_authentication_provider?
            return unprocessable_entity_error!(%s(users.invalid_authentication_provider))
          end

          return true
        end

        def record_attributes_whitelist
          WHITELIST_ATTRIBUTES
        end

        def obtain_password_confirmation
          attributes_hash.fetch(:password_confirmation, true)
        end

        def record_attributes_hash
          attributes = attributes_hash

          attributes[:password_confirmation] ||= obtain_password_confirmation

          attributes
        end

        def attributes_hash
          @options.fetch(:user, {}).to_h.symbolize_keys
        end

        def non_change_trackable_attributes
          [:password_confirmation]
        end

        def updating_profile_image?
          attributes_hash && attributes_hash[:profile_image].present?
        end

        def can_create_staff_user?
          @options[:allow_create_staff_user].present? && valid_key_for_request?
        end

        def valid_key_for_request?
          unless master_api_key.present?
            return forbidden_error!(%s(invalid_master_api_key))
          end

          return (@options[:api_key] == master_api_key)
        end

        def master_api_key
          Application.config.master_api_key
        end

      end
    end
  end
end
