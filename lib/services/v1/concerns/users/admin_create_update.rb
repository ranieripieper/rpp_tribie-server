module Services::V1::Concerns
  module Users
    module AdminCreateUpdate

      extend ActiveSupport::Concern

      included do
        def obtain_password_confirmation
          attributes_hash.fetch(:password_confirmation, nil)
        end

        def address_attributes_for_user
          address_params
        end

        def set_address_for_user(save = true)
          record = (@record || @temp_record)

          if record.address
            record.address.attributes = valid_address_attributes_for_user
          else
            record.address_attributes = valid_address_attributes_for_user
          end

          record.save if save

          record
        end

        def valid_address_attributes_for_user
          address_attributes_for_user.keep_if {|k,v| v.present? }
        end

        def valid_user?
          valid_object?(@user, User)
        end

        def attributes_hash
          @options.fetch(:user, @options).to_h.symbolize_keys
        end

        def invalid_commercial_activity_error!
          admin_error!(:commercial_activity_id)
        end

        def invalid_ie_number_error!(state, number)
          admin_error!(:ie_number)
        end

        def invalid_city_id_error!(options)
          admin_error!(:city_id, 'users.city_id')
        end

        def not_found_city_error!
          admin_error!(:city_id, 'users.city_id')
        end

        def address_error!(attribute, key = nil)
          error = { address: { attribute => translate_error(key || attribute) }}

          return unprocessable_entity_error(error, translate: false)
        end

      end
    end
  end
end
