require 'eventmachine'
require 'ably'

module Services
  module V1
    class Ably::SendMessageService < BaseService
    
      def execute
        execute_action do
          success_response if send_message
        end
      end

      def send_message      
        puts "send message...." 
        @result = false
        EventMachine.run {
          client = ::Ably::Realtime.new(key: Application::Config.ably_key)
          channel = client.channels.get(options[:channel])
          log.info("Enviando mensagem to #{options[:channel]} #{options[:event]}")
          deferrable = channel.publish(options[:event], options[:message]) do
            puts "Messages successfully sent to #{options[:channel]}"
            log.info("Messages successfully sent to #{options[:channel]}")
            @result = true
            EventMachine::stop_event_loop 
          end
          
          deferrable.errback do |err|       
            puts "Unable to publish messages; err = #{err} - channel: #{options[:channel]} - #{err}"
            log.error("Unable to publish messages; err = #{err} - channel: #{options[:channel]} -  #{err}")
            EventMachine::stop_event_loop
          end          
        }        
        
        return @result
        #EventMachine.run unless EventMachine.reactor_running? && EventMachine.reactor_thread.alive?
        #thread.join
      end

      private

      def can_execute?
        return !(options[:channel].nil? && options[:message].nil? && options[:event].nil?)
      end

    end
  end
end
