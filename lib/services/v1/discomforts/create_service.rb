module Services
  class V1::Discomforts::CreateService < BaseCreateService

    NICKNAMES = Application::Config.nicknames.dup
    
    WHITELIST_ATTRIBUTES = [
      :title,
      :description
    ]

    record_type ::Discomfort

    private

    def user_can_create_record?
      return @user.can_create_discomfort?(record_allowed_attributes)
    end

    def attributes_hash
      (@options.fetch(:discomfort, {}) || @options).to_h
    end

    def record_attributes_hash
      @options.fetch(:discomfort, {})
    end

    def record_attributes_hash
      @_record_attributes ||= attributes_hash.symbolize_keys
      @_record_attributes
    end

    def attributes_hash
      @options.fetch(:discomfort, {}).to_h.symbolize_keys
    end

    def record_attributes_whitelist
      WHITELIST_ATTRIBUTES
    end

    def build_record_scope
      @user.discomforts
    end

    def after_success
    end

  end
end
