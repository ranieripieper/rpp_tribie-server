module Services
  module V1
    class Discomforts::FinishTalkingService < BaseUpdateService

      WHITELIST_ATTRIBUTES = [
        :status
      ]

      record_type Discomfort

      after_success do
        params = {}
        params[:discomfort_id] = @record.id
        params[:status] = record_allowed_attributes[:status]
        Workers::V1::FinishTalkingWorker.perform(true, params) 
        if @record.status.to_sym != :mission_accomplished then
          #send mail report
          delivery_async_email(DiscomfortMailer, :report, discomfort_id: @record.id, status: @record.status)
        end

      end
      
      def record_attributes_hash
        options[:discomfort].to_h || {}
      end

      def record_attributes_whitelist
        WHITELIST_ATTRIBUTES
      end
      
      private
      def user_can_update_record?
        status =  record_allowed_attributes[:status].to_sym
        return false if !@record.talking?
        
        if @record.user == @user and ::Discomfort::VALID_STATUST_USER_TO_REPORT.member?(status) then
          return true
        elsif @record.tribier == @user and ::Discomfort::VALID_STATUST_TRIBIER_TO_REPORT.member?(status) then
           return true
        end
        
        return false
       
      end

    end
  end
end
