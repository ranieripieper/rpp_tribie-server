module Services
  module V1
    class Discomforts::DeleteService < BaseDeleteService

      record_type Discomfort

      private
      def user_can_delete_record?
        @record.user_can_delete?(@user)
      end

    end
  end
end
