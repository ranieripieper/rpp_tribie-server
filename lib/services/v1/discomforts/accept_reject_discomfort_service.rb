module Services
  module V1
    class Discomforts::AcceptRejectDiscomfortService < BaseUpdateService

      WHITELIST_ATTRIBUTES = [
        :status
      ]

      record_type Discomfort

      after_success do
        params = {}
        params[:discomfort_id] = @record.id
        params[:accept_talking] = @record.status.to_sym == :talking
        ::Workers::V1::AcceptRejectTalkingWorker.perform(true, params)

        if @record.status.to_sym == :tribier_rejected_with_report then
          #send mail report
          delivery_async_email(DiscomfortMailer, :report, discomfort_id: @record.id, status: @record.status)
        end

      end

      def update_record
        @record = @record.class.send(:update, @record.id, record_allowed_attributes)
        if @record.status.to_sym == :tribier_rejected or @record.status.to_sym == :tribier_rejected_with_report then
          @record.tribier.is_helping = false
          @record.tribier.save
        end

        @record
      end

      def record_attributes_hash
        options.to_h || {}
      end

      def record_attributes_whitelist
        WHITELIST_ATTRIBUTES
      end

      private

      def user_can_update_record?
        @record.status.to_sym == :waiting_response and @record.tribier.id == @user.id
      end

    end
  end
end
