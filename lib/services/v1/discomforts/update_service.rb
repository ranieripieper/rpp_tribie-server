module Services
  module V1
    class Discomforts::UpdateService < BaseUpdateService

      WHITELIST_ATTRIBUTES = [
        :title,
        :description,
        :tribier_id,
        :user_nickname,
        :tribier_nickname,
      ]

      record_type Discomfort

      def record_attributes_hash

        options[:discomfort].to_h || {}
      end

      def record_attributes_whitelist
        WHITELIST_ATTRIBUTES
      end

      private
      def user_can_update_record?
        @record.user_can_update?(@user)
      end

    end
  end
end
